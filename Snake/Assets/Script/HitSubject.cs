﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitSubject : MonoBehaviour
{
    public List<GameObject> PoolObstacle = new List<GameObject>();
    public List<GameObject> PoolFruits = new List<GameObject>();
    [SerializeField] private GameObject[] _wallBush;
    [SerializeField] private GameObject _headSolo;
    [SerializeField] private Vector3 _pointHeadSolo;
    [SerializeField] private Vector3[] _pointWallBush;
    [SerializeField] private Vector3[] _pointObstacle;
    [SerializeField] private int _quantityWallBush = 88;
    private int _invoice = 0;

    public static HitSubject instance;

    private void Awake()
    {
        instance = this;
    }
    void Update()
    {
        AddPointRocks();
        AddPointWallBush();
        AddPointSnake();
        HitObstacle();
        HitWallBush();
        HitTails();
    }

    public void AddPointSnake()
    {
        _pointHeadSolo = _headSolo.transform.position;
    }

    public void TakeFruits()
    {
        for (int i = 0; 0 < PoolFruits.Count; i++)
        {
            if (PoolFruits[i].activeSelf && PoolFruits[i].transform.position == _headSolo.transform.position)
            {
                SnakeController.instance.AddTails();
                PoolFruits[i].SetActive(false);
                SnakeController.instance._audioSource.PlayOneShot(SnakeController.instance._takeFruitsSound);
            }
        }
    }

    public void AddPointRocks()
    {
        int i = 0;
        foreach (GameObject obj in PoolObstacle)
        {
            _pointObstacle[i] = obj.transform.position;
            i++;
            if (i == FieldObjectsGeneration.instance._quantityObstacle)
            {
                break;
            }
        }
    }

    public void AddPointWallBush()
    {
        int i = 0;
        foreach (GameObject obj in _wallBush)
        {
            _pointWallBush[i] = obj.transform.position;
            i++;
            if (i == _quantityWallBush)
            {
                break;
            }
        }
    }
    public void HitObstacle()
    {
        foreach (Vector3 vect in _pointObstacle)
        {
            if (vect == _pointHeadSolo)
            {
                if (_invoice < 1)
                {
                    _invoice++;
                    Debug.Log("Я врезался в камень " + _invoice);
                    StartCoroutine(CancelInvoice());
                    Menu.instance.FastStartGame();
                }
                break;
            }
        }
    }

    public void HitWallBush()
    {
        foreach (Vector3 vect in _pointWallBush)
        {
            if (vect == _pointHeadSolo)
            {
                if (_invoice < 1)
                {
                    _invoice++;
                    Debug.Log("Я врезался в ограждение " + _invoice);
                    StartCoroutine(CancelInvoice());
                    Menu.instance.ShowLoseMenu();
                }
                break;
            }
        }
    }
    

    public void HitTails()
    {
        for (int i = 0; 0 < SnakeController.instance._tails.Count; i++)
        {
            if (SnakeController.instance._tails[i].transform.position == _headSolo.transform.position)
            {
                Menu.instance.ShowLoseMenu();
            }
        }
    }
    
    IEnumerator CancelInvoice()
    {
        yield return new WaitForSeconds(1f);
        _invoice = 0;
    }
}
