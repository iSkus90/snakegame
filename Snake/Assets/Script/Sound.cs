﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sound : MonoBehaviour
{
    [SerializeField] private GameObject[] _sound;
    public static bool _startmenu = true;

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
        RebootingSound();
    }

    public void RebootingSound()
    {
        _sound = GameObject.FindGameObjectsWithTag("Sound");

        if (_sound.Length == 2)
        {
            Destroy(gameObject);
        }
    }
}
