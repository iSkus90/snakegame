﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    [SerializeField] private GameObject _start;
    [SerializeField] private GameObject _lose;
    [SerializeField] private GameObject _win;
    public static Menu instance;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        ShowStartMenu();
    }

    public void ShowStartMenu()
    {
        if (Sound._startmenu==true)
        {
            _start.SetActive(true);
            Sound._startmenu = false;
            Pause();
        }
    }

    public void StartGame()
    {
        _start.SetActive(false);
        Resume();
    }

    public void FastStartGame()
    {
        LoadMainScene();
        Resume();
        _lose.SetActive(false);
        _win.SetActive(false);
        Debug.Log("Resume Win");
    }

    public void ShowLoseMenu()
    {
        _lose.SetActive(true);
        Pause();
    }

    public void ShowWinMenu()
    {
        _win.SetActive(true);
        Pause();
    }

    public void Resume()
    {
        Time.timeScale = 1f;
    }

    public void Pause()
    {
        Time.timeScale = 0f;
    }

    public void LoadMainScene()
    {
        SceneManager.LoadScene("Main");
    }
}
