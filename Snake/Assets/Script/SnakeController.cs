﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SnakeController : MonoBehaviour
{
    [SerializeField] public List<Transform> _tails;
    [SerializeField] private GameObject _bonePrefab;
    [SerializeField] private float _speed;
    [SerializeField] private float _moveTime;
    [SerializeField] private float _speedMove;
    [SerializeField] public AudioSource _audioSource;
    [SerializeField] public AudioClip _takeFruitsSound;
    private Transform _transform;

    public static SnakeController instance;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        _transform = GetComponent<Transform>();
    }

    private void Update()
    {
        MoveSnake();
        WinGameTails();
    }

    public void MoveSnake()
    {
        if (_moveTime > _speedMove)
        {
            MoveSnakeTails(_transform.position + _transform.forward);
            _moveTime = _speed;
            HitSubject.instance.TakeFruits();
        }
        else
        {
            _moveTime += Time.deltaTime;
        }
    }

    public void MoveRight()
    {
        transform.Rotate(0, 90, 0);
    }

    public void MoveLeft()
    {
        transform.Rotate(0, -90, 0);
    }

    private void MoveSnakeTails(Vector3 newPosition)
    {
        Vector3 previosPosition = _transform.position;

        foreach (var bone in _tails)
        {
            var temp = bone.position;
            bone.position = previosPosition;
            previosPosition = temp;
        }
        _transform.position = newPosition;
    }

    public void AddTails()
    {
        var bone = Instantiate(_bonePrefab);
        _tails.Add(bone.transform);
        _speed += 0.1f;
    }

    public void WinGameTails()
    {
        if (_tails.Count >= 10)
        {
            Menu.instance.ShowWinMenu();
            _tails.Clear();
        }

    }
}
