﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FieldObjectsGeneration : MonoBehaviour
{
    [SerializeField] private int _xSize = 7;
    [SerializeField] private int _zSize = 17;
    [SerializeField] public int _quantityObstacle;
    [SerializeField] public int _quantityFruit;
    [SerializeField] private GameObject _containerFruits;
    [SerializeField] private GameObject _containerObstacle;
    [SerializeField] private GameObject[] _obstaclePrefab;
    [SerializeField] private GameObject[] _fruitPrefab;
    private Vector3 _generatePosition;

    public static FieldObjectsGeneration instance;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        InitializeObstacle(_obstaclePrefab);
        InitializeFruit(_fruitPrefab);
    }

    public void InitializeObstacle(GameObject[] prefabs)
    {
        for (int i = 0; i < _quantityObstacle; i++)
        {
            _generatePosition = new Vector3(Random.Range(_xSize * -1, _xSize), 0, Random.Range(3, _zSize));
            int randomIndex = Random.Range(0, prefabs.Length);
            GameObject spawned = Instantiate(prefabs[randomIndex], _generatePosition, Quaternion.identity);
            spawned.transform.parent = _containerObstacle.transform;
            HitSubject.instance.PoolObstacle.Add(spawned);

        }
    }

    public void InitializeFruit(GameObject[] prefabs)
    {
        for (int i = 0; i < _quantityFruit; i++)
        {
            _generatePosition = new Vector3(Random.Range(_xSize * -1, _xSize), 0, Random.Range(1, _zSize));
            int randomIndex = Random.Range(0, prefabs.Length);
            GameObject spawned = Instantiate(prefabs[randomIndex], _generatePosition, Quaternion.Euler(-90,0,0));
            spawned.transform.parent = _containerFruits.transform;
            HitSubject.instance.PoolFruits.Add(spawned);
        }
    }
}
